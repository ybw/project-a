FROM python:3.7

MAINTAINER c.slednev@gmail.com

COPY ./app /code/
WORKDIR /code

# Install requirements
RUN set -x && pip install -v -r requirements.txt

#EXPOSE 8000
#ENTRYPOINT [ "python" ]
#CMD [ "app.py" ]