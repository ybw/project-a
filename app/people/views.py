from rest_framework.viewsets import GenericViewSet, mixins
from people.serializers import PeopleSerializer
from people.models import People


class PeopleViewSet(mixins.RetrieveModelMixin,
                  mixins.ListModelMixin,
                  GenericViewSet):
    serializer_class = PeopleSerializer
    queryset = People.objects.all()
