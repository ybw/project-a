# Generated by Django 3.0 on 2019-12-12 22:46

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('films', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='People',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.TextField(help_text='Name')),
                ('gender', models.TextField(help_text='Gender')),
                ('age', models.TextField(help_text='Age')),
                ('eye_color', models.TextField(help_text='Eye color')),
                ('hair_color', models.TextField(help_text='Hair color')),
                ('films', models.ManyToManyField(help_text='Films', related_name='people', to='films.Film')),
            ],
        ),
    ]
