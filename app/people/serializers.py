__author__ = 'Constantine Slednev <c.slednev@gmail.com>'

from rest_framework import serializers
from people.models import People


class PeopleSerializer(serializers.HyperlinkedModelSerializer):
    films = serializers.HyperlinkedRelatedField(many=True, view_name='film-detail', read_only=True)

    class Meta:
        model = People
        fields = ('id', 'name', 'gender', 'age', 'eye_color', 'hair_color', 'films', 'url',)
