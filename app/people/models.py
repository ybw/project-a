from django.db import models
from django.utils.translation import ugettext_lazy as _
from films.models import Film
import uuid


class People(models.Model):
    '''
    the model contains GHIBLI films
    '''

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(help_text=_('Name'))
    gender = models.TextField(help_text=_('Gender'))
    age = models.TextField(help_text=_('Age'))
    eye_color = models.TextField(help_text=_('Eye color'))
    hair_color = models.TextField(help_text=_('Hair color'))
    films = models.ManyToManyField(Film, related_name='people', help_text=_('Films'))
