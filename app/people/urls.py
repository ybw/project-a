__author__ = 'Constantine Slednev <c.slednev@gmail.com>'

from rest_framework import routers
from people.views import PeopleViewSet

router = routers.SimpleRouter()
router.register('people', PeopleViewSet, 'people')

urlpatterns = router.urls
