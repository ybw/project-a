__author__ = 'Constantine Slednev <c.slednev@gmail.com>'

from rest_framework import serializers
from films.models import Film


class FilmSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Film
        fields = ('id', 'title', 'description', 'director', 'producer', 'release_date', 'rt_score', 'url')
