from django.db import models
from django.utils.translation import ugettext_lazy as _
import uuid


class Film(models.Model):
    '''
    the model contains GHIBLI films
    '''

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.TextField(help_text=_('Title'))
    description = models.TextField(help_text=_('Description'))
    director = models.TextField(help_text=_('Director'))
    producer = models.TextField(help_text=_('Producer'))
    release_date = models.TextField(help_text=_('Release date'))
    rt_score = models.TextField(help_text=_('RT Score'))
