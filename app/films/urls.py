__author__ = 'Constantine Slednev <c.slednev@gmail.com>'

from rest_framework import routers
from films.views import FilmViewSet

router = routers.SimpleRouter()
router.register('films', FilmViewSet, 'film')

urlpatterns = router.urls
