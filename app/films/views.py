from rest_framework.viewsets import GenericViewSet, mixins
from films.serializers import FilmSerializer
from films.models import Film


class FilmViewSet(mixins.RetrieveModelMixin,
                  mixins.ListModelMixin,
                  GenericViewSet):
    serializer_class = FilmSerializer
    queryset = Film.objects.all()
