__author__ = 'Constantine Slednev <c.slednev@gmail.com>'

from apscheduler.schedulers.background import BackgroundScheduler
import requests
from requests.exceptions import ConnectionError
from core.settings import GHIBLI_API_FILMS, GHIBLI_API_PEOPLE
from datetime import datetime
# from django_apscheduler.jobstores import register_events, register_job
import logging

logger = logging.getLogger('django')

scheduler = BackgroundScheduler()


# scheduler.add_jobstore(DjangoJobStore(), 'default')


def update_films():
    from films.models import Film
    try:
        films_response = requests.get(GHIBLI_API_FILMS)
    except ConnectionError:
        # do something
        pass
    else:
        for film in films_response.json():
            film, created = Film.objects.get_or_create(
                id=film['id'],
                defaults=dict(
                    title=film.get('title'),
                    description=film.get('description'),
                    director=film.get('director'),
                    producer=film.get('producer'),
                    release_date=film.get('release_date'),
                    rt_score=film.get('rt_score'),
                )
            )
            if created:
                logger.info(f'Created film with ID {film.id}')
            else:
                logger.info(f'Film with ID {film.id} has already exists')


def update_people():
    from people.models import People
    from films.models import Film
    try:
        people_response = requests.get(GHIBLI_API_PEOPLE)
    except ConnectionError:
        # do something
        pass
    else:
        for person in people_response.json():
            film_ids = list(map(lambda i: i.split('/')[-1], person.get('films')))
            person, created = People.objects.get_or_create(
                id=person['id'],
                defaults=dict(
                    name=person.get('name'),
                    gender=person.get('gender'),
                    age=person.get('age'),
                    eye_color=person.get('eye_color'),
                    hair_color=person.get('hair_color'),
                )
            )
            if created:
                person.films.add(*Film.objects.filter(id__in=film_ids))

            if created:
                logger.info(f'Created person with ID {person.id}')
            else:
                logger.info(f'Person with ID {person.id} has already exists')


def update():
    update_films()
    update_people()


scheduler.add_job(update, 'interval', minutes=1, next_run_time=datetime.now())
scheduler.start()
print('Scheduler started!')
