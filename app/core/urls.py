from django.urls import include, path

from films.urls import urlpatterns as films_urlpatterns
from people.urls import urlpatterns as people_urlpatterns

api_patterns = [
    path('', include(films_urlpatterns)),
    path('', include(people_urlpatterns))
]

urlpatterns = [
    path('movies/', include(api_patterns)),
]

import core.jobs